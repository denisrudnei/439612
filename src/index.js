const ContaResolver = require('./resolvers/ContaResolver')
const { GraphQLServer } = require('graphql-yoga')
const path = require('path')

const server = new GraphQLServer({
  typeDefs: path.resolve(__dirname, 'schema.graphql'),
  resolvers: ContaResolver
})

server.start(() => {
  console.log('server initialized')
})
